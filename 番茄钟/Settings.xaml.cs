﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;

using System.Windows.Threading;
using System.Windows.Navigation;
using System.Windows.Media.Imaging;
using System.Diagnostics;
using System.Threading;

namespace 番茄钟
{
    public partial class Settings : PhoneApplicationPage
    {
        public Settings()
        {
            InitializeComponent();
            
        }

        private void LockOnOffChecked(object sender, RoutedEventArgs e)
        {
            LockModeOnOff.Content = "是";
            try
            {
                Microsoft.Phone.Shell.PhoneApplicationService.Current.ApplicationIdleDetectionMode =
                    Microsoft.Phone.Shell.IdleDetectionMode.Disabled;
            }
            catch (InvalidOperationException ex)
            {
                Debug.WriteLine("Exception while using LockOnOff: " + ex.ToString());
            }
        }

        private void LockOnOffUnchecked(object sender, RoutedEventArgs e)
        {
            LockModeOnOff.Content = "否";
            try
            {
                Microsoft.Phone.Shell.PhoneApplicationService.Current.ApplicationIdleDetectionMode =
                    Microsoft.Phone.Shell.IdleDetectionMode.Enabled;
            }
            catch (InvalidOperationException ex)
            {
                Debug.WriteLine("Exception while using LockOnOff: " + ex.ToString());
            }
        }

    }
}