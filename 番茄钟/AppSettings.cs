﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;

using System.Windows.Threading;
using System.Windows.Navigation;
using System.Windows.Media.Imaging;
using System.Diagnostics;
using System.Threading;
using Coding4Fun.Phone.Controls;
using Coding4Fun.Phone.Controls.Toolkit;

using System.IO.IsolatedStorage;

namespace 番茄钟
{
    public class AppSettings
    {
        IsolatedStorageSettings isolatedStore;

        const string CountDownSpanValueName = "CountDownSpanValue";
        const string RestSpanValueName = "RestSpanValue";
        const string LockModeOnOffCheckName = "LockModeOnOffCheck";
        const string LongBreakSpanValueName = "LongBreakSpanValue";
        const string LongBreakIntervalName = "LongBreakIntervalValue";

        TimeSpan CountDownSpanValueDefault = TimeSpan.FromMinutes(25);
        TimeSpan RestSpanValueDefault = TimeSpan.FromMinutes(5);
        TimeSpan LongBreakSpanValueDefault = TimeSpan.FromMinutes(10);
        string LongBreakIntervalValueDefault = "2";
        bool LockModeOnOffChecckDefault = false;

        public AppSettings()
        {//保存
            try
            {
                isolatedStore = IsolatedStorageSettings.ApplicationSettings;
            }
            catch (Exception e)
            {
                Debug.WriteLine("Exception while using IsolatedStorageSettings: " + e.ToString());
            }
        }

        public bool AddOrUpdateValue(string Key, Object value)
        {
            bool valueChanged = false;

            if (isolatedStore.Contains(Key))
            {
                if (isolatedStore[Key] != value)
                {
                    isolatedStore[Key] = value;
                    valueChanged = true;
                }
            }
            else
            {
                isolatedStore.Add(Key, value);
                valueChanged = true;
            }

            return valueChanged;
        }

        public valueType GetValueOrDefault<valueType>(string Key, valueType defaultValue)
        {//使用泛型
            valueType value;

            if (isolatedStore.Contains(Key))
            {
                value = (valueType)isolatedStore[Key];
            }
            else
            {
                value = defaultValue;
            }

            return value;
        }

        public void Save()
        {
            isolatedStore.Save();
        }

        public TimeSpan CountDownSpanValue
        {
            get
            {
                return GetValueOrDefault<TimeSpan>(CountDownSpanValueName, CountDownSpanValueDefault);
            }
            set
            {
                AddOrUpdateValue(CountDownSpanValueName, value);
                Save();
            }
        }

        public TimeSpan RestSpanValue
        {
            get
            {
                return GetValueOrDefault<TimeSpan>(RestSpanValueName, RestSpanValueDefault);
            }
            set
            {
                AddOrUpdateValue(RestSpanValueName, value);
                Save();
            }
        }

        public TimeSpan LongBreakSpanValue
        {
            get
            {
                return GetValueOrDefault<TimeSpan>(LongBreakSpanValueName, LongBreakSpanValueDefault);
            }
            set
            {
                AddOrUpdateValue(LongBreakSpanValueName, value);
                Save();
            }
        }

        public string LongBreakIntervalValue
        {
            get
            {
                return GetValueOrDefault<string>(LongBreakIntervalName, LongBreakIntervalValueDefault);
            }
            set
            {
                AddOrUpdateValue(LongBreakIntervalName, value);
                Save();
            }
        }

        public bool LockModeOnOffCheck
        {
            get
            {
                return GetValueOrDefault<bool>(LockModeOnOffCheckName, LockModeOnOffChecckDefault);
            }
            set
            {
                AddOrUpdateValue(LockModeOnOffCheckName, value);
                Save();
            }
        }

    }
}
