﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;

using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework;
using System.Windows.Threading;
using System.Windows.Navigation;
using System.Windows.Media.Imaging;
using System.Diagnostics;
using System.Threading;
using Coding4Fun.Phone.Controls;
using Coding4Fun.Phone.Controls.Toolkit;
using Microsoft.Phone.Scheduler;
using Microsoft.Devices;

namespace 番茄钟
{
    public partial class MainPage : PhoneApplicationPage
    {
        // 构造函数
        public MainPage()
        {
            InitializeComponent();
            DataContext = this;
            this.TimeBlock.Text = Aset.CountDownSpanValue.ToString().Substring(3, 2);//后面的2是指长度
            this.SecondBlock.Text = Aset.CountDownSpanValue.ToString().Substring(6, 2);

        }
        #region 各种变量
        static AppSettings Aset = new AppSettings();
        private const string Stop = "停止";
        private const string Start = "开始";
        private const string Working = "番茄时间，请专心工作......";
        private const string OnBreak = "休息时间，喝杯咖啡吧^_^";
        private const string TextDefault = "准备启动下一个番茄......";

        Stopwatch countdownstopwatch = new Stopwatch();
        DispatcherTimer countdowntimer = new DispatcherTimer();
        Stopwatch reststopwatch = new Stopwatch();
        DispatcherTimer resttimer = new DispatcherTimer();

        long TotalTicksRemaining = 0;
        long TotalRestRemaining = 0;
        long TotalLongRemaining = 0;
        int TotalLongCount = 0;
        //PersistSettings<long> CountDownTicks = new PersistSettings<long>("countDownTicks", ((TimeSpan)(Aset.CountDownSpanValue)).Ticks);
        //PersistSettings<long> TotalTicksRemaining = new PersistSettings<long>("totalTicksRemaining", 100);
        //PersistSettings<bool> isTimerRunning = new PersistSettings<bool>("IsTimerRunning", false);

        long CountDownTicks()
        {
            return ((TimeSpan)(Aset.CountDownSpanValue)).Ticks;
        }
        long RestTicks()
        {
            return ((TimeSpan)(Aset.RestSpanValue)).Ticks;
        }
        long LongRestTicks()
        {
            return ((TimeSpan)(Aset.LongBreakSpanValue)).Ticks;
        }
        int LongCount()
        {
            return int.Parse((string)(Aset.LongBreakIntervalValue));
        }
        
        #endregion

        private void StartButton_Click(object sender, RoutedEventArgs e)
        {
            //String name = System.Guid.NewGuid().ToString();

            switch (((Button)sender).Content.ToString())
            {
                case Start:
                    {
                        TotalTicksRemaining = CountDownTicks();
                        countdowntimer.Interval = new TimeSpan(0, 0, 0, 0, 1);
                        countdowntimer.Tick += new EventHandler(countdowntimer_Tick);
                        countdowntimer.Start();
                        countdownstopwatch.Reset();
                        countdownstopwatch.Start();
                        this.StatusBlock.Text = Working;
                        this.StartButton.Content = Stop;

                        
                        ////Alarm alarm = new Alarm(name);
                        ////alarm.Sound = new Uri("Stopped.wav", UriKind.Relative);
                        ////alarm.BeginTime = DateTime.Now;
                        ////alarm.ExpirationTime = DateTime.Now.AddTicks(TotalTicksRemaining);
                        ////ScheduledActionService.Add(alarm);

                        break;
                    }
                case Stop:
                    {
                        var stopPrompt = new MessagePrompt
                        {
                            Title = "提示",
                            Body = new TextBlock { Text = "确实要停止计时吗？", Foreground = new SolidColorBrush(Colors.Green), FontSize = 30.0, TextWrapping = TextWrapping.Wrap },
                            IsAppBarVisible = true,
                            IsCancelVisible = true
                        };
                        stopPrompt.Show();
                        stopPrompt.Completed += stop_Completed;

                        break;
                    }
            }
        }

        void countdowntimer_Tick(object sender, EventArgs e)
        {
            TotalLongCount++;//计算间隔数
            if (TotalLongCount == (LongCount()+1))
                TotalLongCount = 1;
            long TicksRemaining = TotalTicksRemaining - countdownstopwatch.GetElapsedDateTimeTicks();
            
            if (TicksRemaining < 0)
            {
                countdowntimer.Stop();
                countdownstopwatch.Stop();
                countdownstopwatch = new Stopwatch();
                countdowntimer = new DispatcherTimer();

                var stream = TitleContainer.OpenStream("Stopped.wav");
                var countdownbeep = SoundEffect.FromStream(stream);
                VibrateController vc = VibrateController.Default;
                FrameworkDispatcher.Update();
                countdownbeep.Play();
                vc.Start(TimeSpan.FromMilliseconds(500)); //震动500毫秒
                
                var messagePrompt = new MessagePrompt
                {
                    Title = "提示",
                    Body = new TextBlock { Text = "休息片刻？", Foreground = new SolidColorBrush(Colors.Green), FontSize = 30.0, TextWrapping = TextWrapping.Wrap },
                    IsAppBarVisible = true,
                    IsCancelVisible = true

                };
                messagePrompt.Show();
                messagePrompt.Completed += stringObject_Completed;

            }
            else
            {
                TimeSpan newTime = new TimeSpan(TicksRemaining);
                TimeBlock.Text = new TimeSpan(newTime.Hours, newTime.Minutes, newTime.Seconds).ToString().Substring(3, 2);
                SecondBlock.Text = new TimeSpan(newTime.Hours, newTime.Minutes, newTime.Seconds).ToString().Substring(6, 2);
            }
        }

        void stringObject_Completed(object sender, PopUpEventArgs<string, PopUpResult> e)
        {
            if (e.PopUpResult == PopUpResult.Ok)
            {
                TotalRestRemaining = RestTicks();
                TotalLongRemaining = LongRestTicks();//长时间休息时长

                resttimer.Interval = new TimeSpan(0, 0, 0, 0, 1);
                resttimer.Tick += new EventHandler(resttimer_Tick);
                resttimer.Start();
                reststopwatch.Reset();
                reststopwatch.Start();

                this.StatusBlock.Text = OnBreak;
                this.StartButton.Content = Stop;
            }
            else
            {
                this.StatusBlock.Text = TextDefault;
                this.StartButton.Content = Start;
                return;
            }
        }

        long RestRemaining=0;
        void resttimer_Tick(object senders, EventArgs es)
        {
            if (TotalLongCount < LongCount())
                RestRemaining = TotalRestRemaining - reststopwatch.GetElapsedDateTimeTicks();
            else
                RestRemaining = TotalLongRemaining - reststopwatch.GetElapsedDateTimeTicks();
            if (RestRemaining < 0)
            {
                resttimer.Stop();
                reststopwatch.Stop();
                reststopwatch = new Stopwatch();
                resttimer = new DispatcherTimer();

                var stream = TitleContainer.OpenStream("Stopped.wav");
                var countdownbeep = SoundEffect.FromStream(stream);
                VibrateController vc = VibrateController.Default;
                FrameworkDispatcher.Update();
                countdownbeep.Play();
                vc.Start(TimeSpan.FromMilliseconds(500)); //震动250毫秒

                var restPrompt = new MessagePrompt
                {
                    Title = "提示",
                    Body = new TextBlock { Text = "准备进行下一个番茄钟？", Foreground = new SolidColorBrush(Colors.Green), FontSize = 30.0, TextWrapping = TextWrapping.Wrap },
                    IsAppBarVisible = true,
                    IsCancelVisible = true
                };

                restPrompt.Show();
                restPrompt.Completed += rest_Completed;
            }
            else
            {
                TimeSpan newTime = new TimeSpan(RestRemaining);
                TimeBlock.Text = new TimeSpan(newTime.Hours, newTime.Minutes, newTime.Seconds).ToString().Substring(3, 2);
                SecondBlock.Text = new TimeSpan(newTime.Hours, newTime.Minutes, newTime.Seconds).ToString().Substring(6, 2);
            }
        }

        void rest_Completed(object senders, PopUpEventArgs<string, PopUpResult> es)
        {
            if (es.PopUpResult == PopUpResult.Ok)
            {
                TotalTicksRemaining = CountDownTicks();
                countdowntimer.Interval = new TimeSpan(0, 0, 0, 0, 1);
                countdowntimer.Tick += new EventHandler(countdowntimer_Tick);
                countdowntimer.Start();

                countdownstopwatch.Reset();
                countdownstopwatch.Start();

                this.StatusBlock.Text = Working;
                this.StartButton.Content = Stop;
            }
            else
            {
                this.StatusBlock.Text = TextDefault;
                this.StartButton.Content = Start;
                return;
            }
        }

        void stop_Completed(object sender, PopUpEventArgs<string, PopUpResult> e)
        {
            if (e.PopUpResult == PopUpResult.Ok)
            {
                this.StatusBlock.Text = TextDefault;
                this.StartButton.Content = Start;
                countdowntimer.Stop();
                countdownstopwatch.Stop();
                countdownstopwatch = new Stopwatch();
                countdowntimer = new DispatcherTimer();

                resttimer.Stop();
                reststopwatch.Stop();
                reststopwatch = new Stopwatch();
                resttimer = new DispatcherTimer();
                this.TimeBlock.Text = "00";
                this.SecondBlock.Text = "00";

                TotalLongCount = 0;
            }
            else
                return;
        }

        private void Settings_Click(object sender, EventArgs e)
        {
            this.NavigationService.Navigate(new Uri("/Settings.xaml", UriKind.Relative));
        }

        private void About_Click(object sender, EventArgs e)
        {
            this.NavigationService.Navigate(new Uri("/YourLastAboutDialog;component/AboutPage.xaml", UriKind.Relative));
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {//这样使得从设置页面导航回主页面时，TimeBlock上的时间会自动更新
            TimeBlock.Text = Aset.CountDownSpanValue.ToString().Substring(3, 2);
            SecondBlock.Text = Aset.CountDownSpanValue.ToString().Substring(6, 2);
        }
    }
}